(function () {
  var btnFilter = document.getElementById('btn__filter');

  if (btnFilter) {
    var currentTitle = btnFilter.textContent;
    var toggleTitle = btnFilter.getAttribute('data-toggle-title');
    var cntAside = document.getElementById('cnt__aside');
    var body = document.body;
    var state = '';
    btnFilter.setAttribute('data-state', 'disabled');
    btnFilter.addEventListener('click', function() {
      state = this.getAttribute('data-state');
      if (state === 'disabled') {
        this.setAttribute('data-state', 'enabled');
        cntAside.classList.add('aside--active');
        body.classList.add('no-scroll');
        this.textContent = toggleTitle;
      } else if (state === 'enabled') {
        this.setAttribute('data-state', 'disabled');
        cntAside.classList.remove('aside--active');
        body.classList.remove('no-scroll');
        this.textContent = currentTitle;
      }
    });
  }
})();