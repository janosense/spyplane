(function () {
  var cntMain = document.getElementById('cnt__main');
  var cntAside = document.getElementById('cnt__aside');

  if (cntMain && cntAside) {

    changeHeight();
    window.addEventListener('resize', changeHeight);

    function changeHeight() {
      cntMain.style.minHeight = '';
      cntAside.style.minHeight = '';
      var heightAside = cntAside.offsetHeight;
      var heightMain = cntMain.offsetHeight;
      var windowWidth = document.documentElement.clientWidth;
      if (heightAside > heightMain && windowWidth > 1020) {
        cntMain.style.minHeight = heightAside - 20 + 'px'; //20 - padding bottom in dashboard.sass
        cntAside.style.minHeight = '';
      } else if (heightAside < heightMain && windowWidth > 1020) {
        cntAside.style.minHeight = heightMain + 'px';
        cntMain.style.minHeight = '';
      }
    }
  }
})();