(function () {
  var velocityRanges = document.getElementsByClassName('campaigns__velocity-range');

  if (velocityRanges.length) {
    for (var i = 0; i < velocityRanges.length; i++) {
      var range = velocityRanges[i].getAttribute('data-velocity-range');
      if (range !== false) {
        velocityRanges[i].style.width = range * 10 + '%';
      }
    }
  }
})();