(function () {
  var rangeScale = document.getElementById('cnt__range-scale');
  var inputRange = document.getElementById('velocity-range');


  if (rangeScale && inputRange) {
    var buttonRange = document.getElementById('btn__range');
    var indicator = document.getElementById('cnt__indicator');
    var currentRange = inputRange.value;
    if (currentRange !== 0) {
      buttonRange.textContent = currentRange;
      var rightEdge = rangeScale.offsetWidth - buttonRange.offsetWidth;
      buttonRange.style.left = rightEdge * currentRange * 10 / 100 + 'px';
      indicator.style.width = rightEdge * currentRange * 10 / 100 + 10 + 'px';

    }

    buttonRange.textContent = currentRange;

    buttonRange.onmousedown = function(e) {
      var thumbCoords = getCoordinates(buttonRange);
      var shiftX = e.pageX - thumbCoords.left;
      var scaleCoordinates = getCoordinates(rangeScale);
      document.onmousemove = function(e) {
        var newLeft = e.pageX - shiftX - scaleCoordinates.left;
        if (newLeft < 0) {
          newLeft = 0;
        }
        var rightEdge = rangeScale.offsetWidth - buttonRange.offsetWidth;
        if (newLeft > rightEdge) {
          newLeft = rightEdge;
        }
        buttonRange.style.left = newLeft + 'px';
        indicator.style.width = newLeft + 10 + 'px';
        rangeValue = Math.round((newLeft * 100 / rightEdge) / 10);
        buttonRange.textContent = rangeValue;
        inputRange.value = rangeValue;
      };
      document.onmouseup = function() {
        document.onmousemove = document.onmouseup = null;
      };
      return false;
    };
    buttonRange.ondragstart = function() {
      return false;
    };

    // rangeScale.addEventListener('touchstart', function(e) {
    //
    //   console.log(e.offsetX);
    //   var newLeft = e.offsetX;
    //   if (newLeft) {
    //     var buttonRange = document.getElementById('btn__range');
    //     var indicator = document.getElementById('cnt__indicator');
    //     var currentRange = 1;
    //     if (currentRange !== 0) {
    //       buttonRange.textContent = currentRange;
    //       var rightEdge = rangeScale.offsetWidth - buttonRange.offsetWidth;
    //       buttonRange.style.left = rightEdge * currentRange * 10 / 100 + 'px';
    //       indicator.style.width = rightEdge * currentRange * 10 / 100 + 10 + 'px';
    //     }
    //   }
    // });

    function getCoordinates(elem) {
      var box = elem.getBoundingClientRect();
      return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
      };
    }
  }
})();