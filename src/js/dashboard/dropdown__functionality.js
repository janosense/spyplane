(function () {
  var dropdownsList = document.getElementsByClassName('dropdown');//dropdowns list
  if (dropdownsList.length) {
    for (var i = 0; i < dropdownsList.length; i++) {
      dropdownsList[i].setAttribute('data-state', 'disabled');
      dropdownsList[i].addEventListener('click', function(event) {
        var target = event.target;
        var type = this.getAttribute('data-type');
        if (target.hasAttribute('data-list')) {
          var listId = target.getAttribute('data-list');
          if (listId) {
            var list = document.getElementById(listId);
            var dropdownState = this.getAttribute('data-state');
            if (dropdownState === 'disabled') {
              for (var i = 0; i < dropdownsList.length; i++) {
                dropdownsList[i].setAttribute('data-state', 'disabled');
              }
              this.setAttribute('data-state', 'enabled');
              list.onchange = function() {
                if (type === 'dropdown-singular') {
                  for (var i = 0; i < list.elements.length; i++) {
                    if (list.elements[i].checked) {
                      target.textContent = list.elements[i].nextElementSibling.textContent;
                    }
                  }
                } else if (type === 'dropdown-multiple') {
                  var title = '';
                  var checkedCount = 0;
                  for (var j = 0; j < list.elements.length; j++) {
                    if (list.elements[j].checked) {
                      title = list.elements[j].nextElementSibling.textContent;
                      checkedCount++;
                      if (checkedCount > 1) break;
                    }
                  }
                  if (checkedCount === 0) {
                    target.textContent = 'Select from list'
                  } else if (checkedCount === 1) {
                    target.textContent = title;
                  } else {
                    target.textContent = 'Multiple choice';
                  }
                }
              };
            } else {
              this.setAttribute('data-state', 'disabled');
            }
          }
        }
      });
    }
  }
})();