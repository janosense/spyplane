(function () {
  var campaignsLayouts = document.getElementsByClassName('campaigns__layouts');
  if (campaignsLayouts.length) {
    for (var i = 0; i < campaignsLayouts.length; i++) {
      var layoutCount = campaignsLayouts[i].getAttribute('data-count');
      for (var j = 1; j <= layoutCount; j++) {
        var span = document.createElement('SPAN');
        span.classList.add('campaigns__layout-' + j);
        campaignsLayouts[i].appendChild(span);
      }
    }
  }
})();