(function () {
  var verticals = document.getElementsByClassName('stats__verticals-range');
  if (verticals.length) {
    for (var i = 0; i < verticals.length; i++) {
      var currentValue = verticals[i].getAttribute('data-value');
      verticals[i].style.height = currentValue * 10 + 'px'
    }
  }
})();