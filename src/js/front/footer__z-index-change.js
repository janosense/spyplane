(function () {
    var cnt__footer = document.getElementById('cnt__footer');
    var cnt__introduction = document.getElementById('cnt__introduction');
    if (cnt__footer && cnt__introduction) {

        var is_on_scroll = false;
        var window_height = document.documentElement.clientHeight + 200;

        document.addEventListener('scroll', function () {
            scroll_top = window.pageYOffset;

            if (scroll_top > window_height) {
                cnt__footer.style.zIndex = 9;
                cnt__footer.style.opacity = 1;
                cnt__introduction.style.opacity = 0;
                is_on_scroll = true;
            } else if (scroll_top < window_height + 300 && is_on_scroll) {
                cnt__footer.style.zIndex = 7;
                cnt__footer.style.opacity = 0;
                cnt__introduction.style.opacity = 1;
                is_on_scroll = false;
            }

        });
    }
})();