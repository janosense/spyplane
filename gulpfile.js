'use strict'

const gulp = require('gulp');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const csso = require('gulp-csso');
const uncss = require('gulp-uncss');
const sass = require('gulp-sass');
const sasslint = require('gulp-sass-lint');
const htmlmin = require('gulp-htmlmin');
const htmlhint = require('gulp-htmlhint');
const nunjucks = require('gulp-nunjucks-render');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const del = require('del');
const browserSync = require('browser-sync').create();

const path = {
    build: {
        css: 'build/css/',
        js: 'build/js/',
        html: 'build/',
        img: 'build/img',
        fonts: 'build/fonts'
    },
    src: {
        sass: 'src/sass/*.sass',
        js: {
            front: ['src/js/front/*.js'],
            dashboard: 'src/js/dashboard/*.js',
        },
        html: 'src/*.html',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        libs: {}
    },
    watch: {
        sass: 'src/sass/**/*.sass',
        js: 'src/js/**/*js',
        html: 'src/**/*.html',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    remove: 'build/',
    sourcemaps: '../sourcemaps'
};

gulp.task('js:build.front', function() {
    return gulp.src(path.src.js.front)
        .pipe(sourcemaps.init())
        .pipe(concat('main.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write(path.sourcemaps))
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream());
});

gulp.task('js:build.dashboard', function() {
    return gulp.src([path.src.js.dashboard])
        .pipe(sourcemaps.init())
        .pipe(concat('dashboard.min.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write(path.sourcemaps))
        .pipe(gulp.dest(path.build.js))
        .pipe(browserSync.stream());
});

gulp.task('css:build', function () {
    return gulp.src(path.src.sass)
        .pipe(sasslint())
        .pipe(sasslint.format())
        .pipe(sasslint.failOnError())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([autoprefixer({
            browsers: ['last 2 versions']
        })]))
        .pipe(csso())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write(path.sourcemaps))
        .pipe(gulp.dest(path.build.css))
        .pipe(browserSync.stream());
});

gulp.task('html:build', function () {
    return gulp.src(path.src.html)
        .pipe(nunjucks({
            path: ['src/']
        }))
        .pipe(htmlmin({
            removeComments: true,
            collapseWhitespace: true,
            collapseInlineTagWhitespace: true,
            conservativeCollapse: true,
            preserveLineBreaks: true
        }))
        .pipe(htmlhint())
        .pipe(htmlhint.reporter())
        .pipe(gulp.dest(path.build.html))
        .pipe(browserSync.stream());
});

gulp.task('img:build', function() {
    console.log('Use https://kraken.io/ ;)');
    return gulp.src(path.src.img)
        .pipe(gulp.dest(path.build.img));
});

gulp.task('fonts:build', function() {
    return gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('serve', function() {
    browserSync.init({
        server: 'build',
        notify: false
    });

    browserSync.watch('build/**/*.*').on('change', browserSync.reload);
});

gulp.task('remove:build', function() {
    del(path.remove);
});

gulp.task('watch', function() {
    gulp.watch(path.watch.sass, gulp.series('css:build'));
    gulp.watch(path.watch.js, gulp.series(['js:build.front', 'js:build.dashboard']));
    gulp.watch(path.watch.html, gulp.series('html:build'));
    gulp.watch(path.watch.img, gulp.series('img:build'));
    gulp.watch(path.watch.fonts, gulp.series('fonts:build'));
});

gulp.task('build', gulp.series(
    'html:build',
    'css:build',
    'js:build.front',
    'js:build.dashboard',
    'img:build',
    'fonts:build'
));

gulp.task('default', gulp.series(
    'build',
    gulp.parallel(
        'serve',
        'watch')
));